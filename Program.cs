﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Eyes_Tracker
{
    class Program
    {
        const int ServerPort = 4242;
        const string ServerAddr = "127.0.0.1";
        static string carpeta;

        [STAThread]
        static void Main(string[] args)
        {

            bool exit_state = false;
            int startindex, endindex;
            TcpClient gp3_client;
            NetworkStream data_feed;
            StreamWriter data_write;
            String incoming_data = "";

            List<double> X = new List<double>();
            List<double> Y = new List<double>();

            ConsoleKeyInfo keybinput;

            Console.WriteLine("Bienvenido a Eyes Tracker de Innova");
            Console.WriteLine("Por favor oprima ENTER para empezar a recopilar los datos y vuelva a oprimir \nENTER para detener la recopilacion de datos");
            Console.Read();

            try
            {
                gp3_client = new TcpClient(ServerAddr, ServerPort);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to connect with error: {0}", e);
                return;
            }

            data_feed = gp3_client.GetStream();
            data_write = new StreamWriter(data_feed);
            StringBuilder cvscon = new StringBuilder();

            cvscon.AppendLine("FPOGX,FPOGY, Valido, LPOGX, LPOGY, Valido, RPOGX, RPOGY, Valido, BPOGX, BPOGY, Valido,LPCX, LPCY,Valido, RPCX, RPCY,Valido,CX,CY,CS,Tiempo");

            data_write.Write("<SET ID=\"ENABLE_SEND_TIME\" STATE=\"1\" />\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_POG_FIX\" STATE=\"1\" />\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_POG_LEFT\" STATE=\"1\" />\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_POG_RIGHT\" STATE=\"1\" />\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_POG_BEST\" STATE=\"1\" />\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_PUPIL_LEFT\" STATE=\"1\">\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_PUPIL_RIGHT\" STATE=\"1\">\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_CURSOR\" STATE=\"1\" />\r\n");
            data_write.Write("<SET ID=\"ENABLE_SEND_DATA\" STATE=\"1\" />\r\n");

            //  data_write.Write("<SET ID=\"ENABLE_SEND_COUNTER\" STATE=\"1\" />\r\n");


            // Flush the buffer out the socket
            data_write.Flush();

            do
            {
                int ch = data_feed.ReadByte();
                if (ch != -1)
                {
                    incoming_data += (char)ch;

                    // find string terminator ("\r\n") 
                    if (incoming_data.IndexOf("\r\n") != -1)
                    {
                        // only process DATA RECORDS, ie <REC .... />
                        if (incoming_data.IndexOf("<REC") != -1)
                        {
                            double time_val;
                            double fpogx, bpogx, lpogx, rpogx, lpcx, rpcx;
                            double fpogy, bpogy, lpogy, rpogy, lpcy, rpcy;
                            double cx;
                            double cy;
                            int fpog_valid, bpog_valid, lpog_valid, rpog_valid, lpv_valid, rpv_valid, cs;


                            Console.WriteLine("Raw data: {0}", incoming_data);

                            // Process incoming_data string to extract FPOGX, FPOGY, etc...
                            startindex = incoming_data.IndexOf("TIME=\"") + "TIME=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            time_val = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("FPOGX=\"") + "FPOGX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            fpogx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("FPOGY=\"") + "FPOGY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            fpogy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("FPOGV=\"") + "FPOGV=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            fpog_valid = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("LPOGX=\"") + "LPOGX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            lpogx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("LPOGY=\"") + "LPOGY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            lpogy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("LPOGV=\"") + "LPOGV=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            lpog_valid = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("RPOGX=\"") + "RPOGX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            rpogx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("RPOGY=\"") + "RPOGY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            rpogy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("RPOGV=\"") + "RPOGV=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            rpog_valid = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("BPOGX=\"") + "BPOGX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            bpogx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("BPOGY=\"") + "BPOGY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            bpogy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("BPOGV=\"") + "BPOGV=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            bpog_valid = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("LPCX=\"") + "LPCX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            lpcx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("LPCY=\"") + "LPCY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            lpcy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("LPV=\"") + "LPV=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            lpv_valid = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("RPCX=\"") + "RPCX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            rpcx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("RPCY=\"") + "RPCY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            rpcy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("RPV=\"") + "RPV=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            rpv_valid = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("CX=\"") + "CX=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            cx = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("CY=\"") + "CY=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            cy = Double.Parse(incoming_data.Substring(startindex, endindex - startindex));

                            startindex = incoming_data.IndexOf("CS=\"") + "CS=\"".Length;
                            endindex = incoming_data.IndexOf("\"", startindex);
                            cs = Int32.Parse(incoming_data.Substring(startindex, endindex - startindex));


                            cvscon.AppendLine(Convert.ToString(fpogx) + "," + Convert.ToString(fpogy) + "," + Convert.ToString(fpog_valid) + "," +
                                              Convert.ToString(lpogx) + "," + Convert.ToString(lpogy) + "," + Convert.ToString(lpog_valid) + "," +
                                              Convert.ToString(rpogx) + "," + Convert.ToString(rpogy) + "," + Convert.ToString(rpog_valid) + "," +
                                              Convert.ToString(bpogx) + "," + Convert.ToString(bpogy) + "," + Convert.ToString(bpog_valid) + "," +
                                              Convert.ToString(lpcx) + "," + Convert.ToString(lpcy) + "," + Convert.ToString(lpv_valid) + "," +
                                              Convert.ToString(rpcx) + "," + Convert.ToString(rpcy) + "," + Convert.ToString(rpv_valid) + "," +
                                              Convert.ToString(cx) + "," + Convert.ToString(cy) + "," + Convert.ToString(cs)+","+DateTime.Now.ToLongTimeString());


                        }

                        incoming_data = "";

                    }
                }

                if (Console.KeyAvailable == true)
                {
                    keybinput = Console.ReadKey(true);
                    if (keybinput.Key != ConsoleKey.Q)
                    {
                        exit_state = true;
                    }
                }
            }
            while (exit_state == false);

            data_write.Close();
            data_feed.Close();
            gp3_client.Close();


            SaveFileDialog salvar = new SaveFileDialog();
            salvar.Filter = "Archivos csv(*.csv)|*.csv";
            salvar.Title = "Guardar";
            if (salvar.ShowDialog() == DialogResult.OK)
            {
                carpeta = salvar.FileName;

            }

            salvar.Dispose();

            carpeta = carpeta.Replace("\\", "\\\\");
            File.AppendAllText(carpeta, cvscon.ToString());
        }
    }
}
